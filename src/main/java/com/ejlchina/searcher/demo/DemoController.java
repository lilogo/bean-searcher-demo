package com.ejlchina.searcher.demo;

import com.ejlchina.searcher.SearchResult;
import com.ejlchina.searcher.Searcher;
import com.ejlchina.searcher.util.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


@Controller
public class DemoController {

	@Autowired
	private Searcher searcher;

	/**
	 * Demo 首页
	 */
	@GetMapping("/")
	public String index() {
		return "index";
	}

	/**
	 * 列表检索接口
	 */
	@ResponseBody
	@GetMapping("/employee/index")
	public Object index(HttpServletRequest request) {
		// 组合检索、排序、分页 和 统计 都在这一句代码中实现了
		return searcher.search(Employee.class,				// 指定实体类
				MapUtils.flat(request.getParameterMap()), 	// 收集页面请求参数
				new String[] { "age" });					// 统计字段：年龄
	}

/*
	// 以下的代码是一种等效的写法

	@ResponseBody
	@GetMapping("/employee/index")
	public SearchResult<Employee> index(String name, String department, Integer page, Integer size, String sort, String order,
			@RequestParam("name-op") String name_op,
			@RequestParam("name-ic") String name_ic,
			@RequestParam("age-0") Integer age_0,
			@RequestParam("age-1") Integer age_1,
			@RequestParam("age-op") String age_op,
			@RequestParam("department-op") String department_op,
			@RequestParam("department-ic") String department_ic,
			@RequestParam("entryDate-0") String entryDate_0,
			@RequestParam("entryDate-1") String entryDate_1,
			@RequestParam("entryDate-op") String entryDate_op) {
		Map<String, Object> params = new HashMap<>();
		params.put("name", name);					// 姓名检索值
		params.put("name-op", name_op);				// 姓名检索运算符
		params.put("name-ic", name_ic);				// 姓名是否忽略大小写
		params.put("age-0", age_0);					// 年龄第一个检索值
		params.put("age-1", age_1);					// 年龄第二个检索值
		params.put("age-op", age_op);				// 年龄检索运算符
		params.put("department", department);		// 部门检索值
		params.put("department-op", department_op);	// 部门检索运算符
		params.put("department-ic", department_ic);	// 部门是否忽略大小写
		params.put("entryDate-0", entryDate_0);		// 入职时间第一个检索值
		params.put("entryDate-1", entryDate_1);		// 入职时间第二个检索值
		params.put("entryDate-op", entryDate_op);	// 入职时间检索运算符
		params.put("page", page);					// 分页页数
		params.put("size", size);					// 分页大小
		params.put("sort", sort);					// 排序字段
		params.put("order", order);					// 排序方法：asc|desc
		// 组合检索、排序、分页 和 统计 都在这一句代码中实现了
		return searcher.search(Employee.class, params, new String[] { "age" });
	}
*/


}
